��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  m  �     B  1   N  *   �  	   �     �     �     �     �     �     �     �               .  	   ?  
   I  	   T     ^  1   k     �     �                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:04+0100
Last-Translator: Wallon
Language-Team: Hungarian (https://app.transifex.com/anticapitalista/teams/10162/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 Hozzáadás Használaton kívüli szolgáltatás hozzáadása Már minden szolgáltatás be van töltve. Letiltás Nem fut Engedélyezés Napló Nem Újratöltés Eltávolítás Újraindítás Runit szolgáltatáskezelő Szolgáltatás Szolgáltatások Indítás Indítás: Állapot: Leállítás Ez ALAPVETŐ szolgáltatás (nem kapcsolható ki) Fut Igen 