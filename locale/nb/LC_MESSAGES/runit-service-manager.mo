��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  u  �     J     S  "   l     �     �     �     �     �     �     �     �     �     �  	   �       	               3        S     W                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:04+0100
Last-Translator: Wallon
Language-Team: Norwegian Bokmål (https://app.transifex.com/anticapitalista/teams/10162/nb/)
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 Legg til Legg til ubrukt tjeneste Alle tjenester er allerede lastet. Slå av Ned Slå på Logg Nei Last inn på nytt Fjern Starte på nytt Runit tjenestebehandling Tjeneste Tjenester Start Oppstart: Status: Stopp Dette er en VIKTIG tjeneste (den kan ikke slås av) Opp Ja 