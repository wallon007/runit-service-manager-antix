��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  i  �  	   >     H     f     �     �     �     �     �     �  	   �     �     �     �     �     �  	   �            B        V     ]                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:04+0100
Last-Translator: Wallon
Language-Team: Dutch (https://app.transifex.com/anticapitalista/teams/10162/nl/)
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 Toevoegen Ongebruikte service toevoegen Alle services zijn al geladen. Uitschakelen Omlaag Inschakelen Log Nee Herladen Verwijder Herstart Voer Servicemanager uit Dienst Diensten Starten Opstarten Status: Stoppen Dit is een ESSENTIËLE dienst (deze kan niet worden uitgeschakeld) Omhoog Ja 