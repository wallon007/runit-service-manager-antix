��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  �  �  	   �  ;   �  )   �  	   �  
   �     
            
        *  	   2  !   <     ^  	   g     q     y     �     �  4   �     �     �                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:05+0100
Last-Translator: Wallon
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 3.2.2
 Adicionar Adicionar um serviço que ainda não esteja sendo utilizado Todos os serviços já estão carregados. Desativar Desativado Ativar Registro Não Recarregar Remover Reiniciar Gerenciador de Serviços do Runit Serviço Serviços Iniciar Inicializar automaticamente: Estado: Parar Este é um serviço VITAL (não pode ser desativado) Ativado Sim 