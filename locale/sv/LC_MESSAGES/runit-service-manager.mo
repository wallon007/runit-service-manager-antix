��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  k  �  
   @     K  !   k  
   �     �     �     �     �     �     �  	   �     �     �  	   �     �     �             6        E     I                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:05+0100
Last-Translator: Wallon
Language-Team: Swedish (https://app.transifex.com/anticapitalista/teams/10162/sv/)
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 Lägg till Lägg till icke använd tjänst Alla tjänster är redan laddade. Avaktivera Ner Aktivera Logg Nej Läs om Ta bort Starta om Runit Tjänstehanterare Tjänst Tjänster Starta Start: Status: Stopp Detta är en VIKTIG tjänst (den kan inte stängas av) Upp Ja 